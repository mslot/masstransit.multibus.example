#+TITLE: Problem

If you start up rabbitmq, and start the project i would expect to see that the connections in the connections tab of rabbitmq management would get:

1. two connections, but
2. when the timer in the hosted service calls stop, it should go down by one (this is actually happening), BUT
3. when the timer starts the HostedServiceBus, i don't get a second connection again, in rabbitmq
