using System;
using System.Reflection.Metadata;
using System.Threading;
using System.Threading.Tasks;
using MassTransit;
using MassTransit.Registration;
using Microsoft.Extensions.Hosting;

namespace MassTransitTest
{
    public class HostedService : IHostedService, IDisposable
    {
        private Timer _timer = null;
        private readonly IHostedServiceBus _hostedServiceBus;
        private IBusInstance<IHostedServiceBus> _instance;

        public HostedService(IHostedServiceBus hostedServiceBus, IBusInstance<IHostedServiceBus> instance)
        {
            _hostedServiceBus = hostedServiceBus;
            _instance = instance;
        }
        
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.FromSeconds(20), TimeSpan.FromSeconds(60));
            return Task.CompletedTask;
        }

        private void DoWork(object? state)
        {
            try
            {
                _instance.BusControl.Start();
                _instance.BusControl.CheckHealth();
            }
            catch (Exception e)
            {
               Console.WriteLine("error starting"); 
            }
            Console.WriteLine("Started");
            Thread.Sleep(TimeSpan.FromSeconds(50));
            Console.WriteLine("Stopping");
            
            try
            {
                _instance.BusControl.Stop();
            }
            catch (Exception e)
            {
               Console.WriteLine("error stopping"); 
            }
            Console.WriteLine("Stopped");
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer.Dispose();
        }
    }
}